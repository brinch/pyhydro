

import riemannsolver



class Cell:
    def __init__(self):
        self._number = 0
        self._volume = 0.
        self._mass = 0.
        self._momentum = 0.
        self._energy = 0.
        self._density = 0.
        self._velocity = 0.
        self._pressure = 0.

        self._right_ngb = None
        self._surface_area = 1

GAMMA = 5./3.
scale=1.0
timestep = 0.001
endtime = 0.20
nsteps = np.int(endtime/timestep)
ncells = np.int(100*scale)

solver = riemannsolver.RiemannSolver(GAMMA)


cells = []

for i in range(ncells):
    cell = Cell()
    cell._number = i
    cell._volume = 0.01/scale
    if i < ncells/2:
        cell._mass = 0.01/scale
        cell._energy = 0.01 / (GAMMA-1.)
    else:
        cell._mass = 0.00125/scale
        cell._energy = 0.001 / (GAMMA-1.)
    cell._momentum = 0.
    
    if(i>0):
        cells[-1]._right_ngb = cell

    cells.append(cell)

cells[0]._right_ngb = cells[1]


for i in range(nsteps):

    for cell in cells:
        cell._density = cell._mass / cell._volume
        cell._velocity = cell._momentum / cell._mass
        cell._pressure = (GAMMA-1.)*(cell._energy / cell._volume \
                                     - 0.5 * cell._density \
                                     * cell._velocity * cell._velocity)

    
    for cell in (item for item in cells if item._number != ncells-1):
        densityL = cell._density
        velocityL = cell._velocity
        pressureL = cell._pressure

        densityR = cell._right_ngb._density
        velocityR = cell._right_ngb._velocity
        pressureR = cell._right_ngb._pressure

        
        densitySol, velocitySol, pressureSol, _ = solver.solve(densityL,  \
                                                            velocityL, \
                                                            pressureL, \
                                                            densityR,  \
                                                            velocityR, \
                                                            pressureR, \
                                                            timestep)

        flux_mass = densitySol * velocitySol
        flux_momentum = densitySol * velocitySol * velocitySol + pressureSol
        flux_energy = (pressureSol * GAMMA / (GAMMA-1.) + \
                       0.5 * densitySol * velocitySol * velocitySol) \
                       * velocitySol


        A = cell._surface_area

        
        if cell._number != 0:
            cell._mass = cell._mass - flux_mass * A * timestep
            cell._momentum = cell._momentum - flux_momentum * A * timestep
            cell._energy = cell._energy - flux_energy * A * timestep

        if cell._number != ncells-2:
            cell._right_ngb._mass = cell._right_ngb._mass \
                                + flux_mass * A * timestep
            cell._right_ngb._momentum = cell._right_ngb._momentum \
                                + flux_momentum * A * timestep
            cell._right_ngb._energy = cell._right_ngb._energy \
                                + flux_energy * A * timestep



densities = [cell._density for cell in cells]
plt.plot(np.linspace(0,100,ncells),densities, '.', color='red')

